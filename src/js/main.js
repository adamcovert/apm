$(document).ready(() => {

  /**
   * Open / close navigation menu
   */

  $('.burger').on('click', function () {
    $(this).toggleClass('burger--is-active');
    $('.page-header__bottom').toggleClass('page-header__bottom--is-active');
  });



  /**
   * Swiper sliders
   */

  var testimonialsSlider = new Swiper('.testimonials__slider', {
    navigation: {
      prevEl: '.testimonials__slider-nav .slider-nav__btn--prev',
      nextEl: '.testimonials__slider-nav .slider-nav__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      576: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 4,
        spaceBetween: 40
      }
    }
  });

  var galleryThumbsSlider = new Swiper('.gallery__slider-thumbs', {
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpoints: {
      320: {
        slidesPerView: 4,
        spaceBetween: 10
      },
      576: {
        slidesPerView: 6,
        spaceBetween: 10
      }
    }
  });

  var galleryMainSlider = new Swiper('.gallery__slider-main', {
    navigation: {
      prevEl: '.gallery__slider-nav .slider-nav__btn--prev',
      nextEl: '.gallery__slider-nav .slider-nav__btn--next'
    },
    thumbs: {
      swiper: galleryThumbsSlider
    }
  });
});